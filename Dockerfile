FROM node:lts-slim

MAINTAINER Julian <docker@ic.thejulian.uk>
LABEL MAINTAINER="Julian <docker@ic.thejulian.uk>"
LABEL org.opencontainers.image.title="typescript-base"
LABEL org.opencontainers.image.description="typescript-base"
LABEL org.opencontainers.image.source="https://git.thejulian.uk/ci/typescript-base"
LABEL org.opencontainers.image.authors="Julian <docker@ic.thejulian.uk>"
LABEL org.opencontainers.image.documentation="https://git.thejulian.uk/ci/typescript-base"
LABEL uk.thejulian.dockerclang.version="0.0.1"
LABEL org.opencontainers.image.version="0.0.1"

RUN npm install -g npm
RUN npm install -g typescript ts-loader
RUN npm install -g --save-dev typescript ts-loader
RUN npm install -g minify
